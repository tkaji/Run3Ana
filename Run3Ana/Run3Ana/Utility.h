#ifndef __Run3Ana__Utility_H__
#define __Run3Ana__Utility_H__

namespace MyUtil
{
  // copy from athena/Tracking/TrkEvent/TrkTrackSummary/TrkTrackSummary/TrackSummary.h
  enum DetectorType
    {
      pixelBarrel0 = 0, //!< there are three or four pixel barrel layers (R1/R2)
      pixelBarrel1 = 1,
      pixelBarrel2 = 2,
      pixelBarrel3 = 3,

      pixelEndCap0 = 4, //!< three pixel discs (on each side)
      pixelEndCap1 = 5,
      pixelEndCap2 = 6,

      sctBarrel0 = 7, //!< four sct barrel layers
      sctBarrel1 = 8,
      sctBarrel2 = 9,
      sctBarrel3 = 10,

      sctEndCap0 = 11, //!< and 9 sct discs (on each side)
      sctEndCap1 = 12,
      sctEndCap2 = 13,
      sctEndCap3 = 14,
      sctEndCap4 = 15,
      sctEndCap5 = 16,
      sctEndCap6 = 17,
      sctEndCap7 = 18,
      sctEndCap8 = 19,

      trtBarrel = 20,
      trtEndCap = 21,

      DBM0 = 22,
      DBM1 = 23,
      DBM2 = 24,

      numberOfDetectorTypes = 25

    }; // not complete yet
}

#endif // __Run3Ana__Utility_H__
