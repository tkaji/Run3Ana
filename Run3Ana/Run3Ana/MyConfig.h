#ifndef __Run3Ana__MyConfig_H__
#define __Run3Ana__MyConfig_H__

#include "AsgMessaging/StatusCode.h"
#include "AsgMessaging/MessageCheck.h"
#include "AsgTools/ToolStore.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/StandaloneToolHandle.h"
#include "AsgTools/IAsgTool.h"

//ROOT
#include <Rtypes.h>

//SYSTEM
#include <string>

class MyConfig
{
public:
  MyConfig(const char* configFile) : APP_NAME("MyConfig"), m_myConfigName(configFile){};
  //~MyConfig(){};
  int Initialize();
  //void SetIsData(xAOD::TEvent* event);

  void Print() const;
  int Test(){return 0;}

  Bool_t doGRL                        () const {return m_doGRL;}
  Bool_t doPRW                        () const {return m_doPRW;}
  Bool_t IsData                       () const {return m_IsData;}
  Int_t  GetMaxNEvents                () const {return m_maxNEvents;}
  std::string GetSTConfigPath         () const {return m_stConfigLocation;}
  std::string GetPRWConfPath          () const {return m_prwConfigLocation;}
  std::string GetPRWLumiCalcPath      () const {return m_prwLumiCalcLocation;}
  std::vector<std::string> GetGRLPath () const {return m_grlLocation;}
  
private:
  const char* APP_NAME;

  std::string m_myConfigName;
  std::string m_stConfigLocation;
  std::string m_prwConfigLocation;
  std::string m_prwLumiCalcLocation;
  std::vector<std::string> m_grlLocation;
  
  int m_maxNEvents;
  bool m_IsData;
  bool m_doPRW;
  bool m_doGRL;
};

#endif // __Run3Ana__MyConfig_H__
