#ifndef __Run3Ana__Run3Ana_H__
#define __Run3Ana__Run3Ana_H__

#include "Run3Ana/Utility.h"
#include "Run3Ana/MyConfig.h"
//ATLAS (TOOL)

//ATLAS (EDM)
#include "xAODEventInfo/EventInfo.h"
//#include "xAODEgamma/ElectronContainer.h"
//#include "xAODMuon/MuonContainer.h"
//#include "xAODTruth/TruthParticleContainer.h"
//#include "xAODTracking/TrackParticleContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

#include "AsgMessaging/StatusCode.h"
#include "AsgMessaging/MessageCheck.h"
#include "AsgTools/ToolStore.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/StandaloneToolHandle.h"
#include "AsgTools/IAsgTool.h"

//ROOT
#include <Rtypes.h>

//SYSTEM
#include <string>
#include <stdint.h>
#include <memory>
#include <map>

class MyConfig;
//ATLAS (TOOL)
namespace ST    { class SUSYObjDef_xAOD; }
namespace ST    { struct SystInfo; }

//ATLAS (EDM)
namespace xAOD { class TEvent; }
namespace xAOD { class TStore; }
namespace  InDet{class InDetTrackSelectionTool;}
//ROOT
class TFile;
class TEfficiency;

class Run3Ana
{
public:
  Run3Ana(const char *appName, const char *inputFileName, const char *outputFileName, const char *myconfig = "Run3Ana/Run3Ana_Default.conf" );
  ~Run3Ana();

  void Run();

private:
  /// Called at beginning of processing.
  int begin();
  int processEvent(ULong64_t iEntry);
  
  /// Called at end of processing.
  void finish();
  
  /// Called for every event.
  //bool processEvent(xAOD::TEvent& event);

  ULong64_t nEntries;
  UInt_t RunNumber;
  UInt_t lumiBlock;
  ULong64_t EventNumber;
  std::string MCCampaign;
  
private:
  // The application's name:
  const char* APP_NAME;
  std::map<std::string,bool> slices;
  //std::shared_ptr<MyConfig> m_config;
  std::shared_ptr<MyConfig> m_config;
  
  
  std::shared_ptr<xAOD::TEvent> m_event;
  std::shared_ptr<xAOD::TStore> m_transientStorage;
  //const xAOD::EventInfo* eventInfo = nullptr;
  
  std::string inFile;
  std::string outFile;
  std::string confFile;
  std::shared_ptr<ST::SUSYObjDef_xAOD> m_SUSYTools;
  TFile *curFile;                               // A pointer of current reading file
  std::shared_ptr<TFile> m_outputFile;          // 
  std::shared_ptr<InDet::InDetTrackSelectionTool> m_trackSelectionTool;

};

#endif // __Run3Ana__Run3Ana_H__
