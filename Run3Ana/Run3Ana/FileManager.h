#ifndef __Run3Ana__FileManager_H__
#define __Run3Ana__FileManager_H__

//ROOT

//SYSTEM
#include <string>
#include <vector>

class TFile;

class FileManager {
public:
  FileManager(const char* list);
  FileManager(const std::vector<std::string> list);
  void Initialize();

  std::string nextFile();
  std::string curFile() const;

  static std::vector<std::string> SeparateCommaString(std::string fileArray);

  static bool checkRootFile(const TFile *file, std::string filename = "dummy");
  bool GetNextFile(TFile*& file);
  static void OpenFile(TFile*& file, std::string filename);
  static void SafeDeleteTFile(TFile*& file);

private:
  const char* APP_NAME;
  std::vector<std::string> m_fileList;
  size_t m_curIndex;

};


#endif // __Run3Ana__FileManager_H__
