#include <iostream>
#include <fstream>

//MYCODE
#include "Run3Ana/Run3Ana.h"

//ATLAS (EDM)
//#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODBase/IParticleHelpers.h"
//#include "xAODRootAccess/tools/TReturnCode.h"
#include "xAODMetaData/FileMetaData.h"

//ATLAS (TOOL)
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PathResolver/PathResolver.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "PATInterfaces/CorrectionCode.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"

//ROOT
#include "Rtypes.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
//#include "TEfficiency.h"

//SYSTEM
#include <sys/resource.h>

namespace asg{ANA_MSG_HEADER(msgRun3Ana) ANA_MSG_SOURCE(msgRun3Ana,"Run3Ana")}
using namespace asg::msgRun3Ana;

Run3Ana::Run3Ana(const char *appName, const char *inputFileName, const char *outputFileName, const char* myconfig) :
  APP_NAME(appName),curFile(nullptr)
{
  // input fileList:
  std::cout<<"Input file list : "<<inputFileName<<std::endl;

  inFile = inputFileName;
  outFile = outputFileName;
  confFile = myconfig;
    
  // output file
  m_outputFile.reset(new TFile(outputFileName, "RECREATE"));

  // Config files Manager
  m_config.reset(new MyConfig(myconfig));
}

Run3Ana::~Run3Ana() = default;

void Run3Ana::finish()
{
  ANA_MSG_INFO("Run3Ana::finish");
}

int Run3Ana::begin()
{
  ANA_MSG_INFO("Run3Ana::begin");
    
  // Read configFile
  m_config->Initialize();
  m_event.reset(new xAOD::TEvent(xAOD::TEvent::kClassAccess));
  curFile = TFile::Open(inFile.c_str(), "READ");
    
  ANA_CHECK(m_event->readFrom(curFile));
  m_transientStorage.reset(new xAOD::TStore());
  m_transientStorage->setActive(); 

  nEntries = (m_config->GetMaxNEvents() > 0) ? m_config->GetMaxNEvents() : m_event->getEntries();
  m_event->getEntry(0);

  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK( m_event->retrieve( eventInfo, "EventInfo" ) );
  
  m_trackSelectionTool.reset(new InDet::InDetTrackSelectionTool("InDetTrackSelectionToolInRun3Ana", "Loose"));
  ANA_CHECK(m_trackSelectionTool->initialize());
  
  m_SUSYTools.reset(new ST::SUSYObjDef_xAOD(std::string(APP_NAME) + "SUSYTools"));
  ANA_CHECK(m_SUSYTools->setProperty("DataSource", m_config->IsData() ? ST::ISUSYObjDef_xAODTool::Data : ST::ISUSYObjDef_xAODTool::FullSim));
  ANA_CHECK(m_SUSYTools->setProperty("ConfigFile", m_config->GetSTConfigPath()));

  if(!m_config->IsData()){
  //if(false){
    const xAOD::FileMetaData *fMeta = nullptr;
    ANA_CHECK(m_event->retrieveMetaInput(fMeta, "FileMetaData"));
    fMeta->value(xAOD::FileMetaData::mcCampaign, MCCampaign);
  
    if(m_config->doPRW()){
      if(m_config->GetPRWConfPath() == "auto"){
	ANA_CHECK(m_SUSYTools->setBoolProperty("AutoconfigurePRWTool", true));
	ANA_CHECK(m_SUSYTools->setBoolProperty("PRWUseCommonMCFiles", true));
      }else{
	ANA_CHECK(m_SUSYTools->setProperty<std::vector<std::string>>("PRWConfigFiles"  , std::vector<std::string>{m_config->GetPRWConfPath()}));
      }

      if(m_config->GetPRWLumiCalcPath() == "auto"){
	if(MCCampaign == "mc23d"){
	  ANA_CHECK(m_SUSYTools->setProperty<std::vector<std::string>>("PRWLumiCalcFiles", std::vector<std::string>{"Run3Ana/GRL/data23_13p6TeV/20230828/ilumicalc_histograms_None_451587-456749_OflLumi-Run3-003.root"}));
	}else if(MCCampaign == "mc23a"){
	  ANA_CHECK(m_SUSYTools->setProperty<std::vector<std::string>>("PRWLumiCalcFiles", std::vector<std::string>{"Run3Ana/GRL/data22_13p6TeV/20230207/ilumicalc_histograms_None_431810-440613_OflLumi-Run3-003.root"}));	  
	}else{
	  ANA_MSG_ERROR("No corresponding PRWConfigFiles.");
	  abort();
	}
      }else{
	ANA_CHECK(m_SUSYTools->setProperty<std::vector<std::string>>("PRWLumiCalcFiles", std::vector<std::string>{m_config->GetPRWLumiCalcPath()}));
      }
    }
  }
  ANA_CHECK(m_SUSYTools->initialize().isSuccess())
    
  return 0;
}

int Run3Ana::processEvent(ULong64_t iEntry)
{
  m_event->getEntry(iEntry);

  ANA_CHECK(m_SUSYTools->ApplyPRWTool());
  
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK( m_event->retrieve( eventInfo, "EventInfo" ) );
  //eventInfo->auxdecor<unsigned int>("RandomRunNumber") = 456749;

  bool isCleanEvent = !((eventInfo->errorState(xAOD::EventInfo::LAr)  == xAOD::EventInfo::Error ) ||
			(eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ) ||
			(eventInfo->errorState(xAOD::EventInfo::SCT)  == xAOD::EventInfo::Error ) || 
			(eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)));
  if(!isCleanEvent)
    return 0;
  
  // Trigger
  bool passMETtrig = m_SUSYTools->IsMETTrigPassed();
  //m_SUSYTools->GetRunNumber();
  
  // Electron
  xAOD::ElectronContainer* electrons(nullptr);
  xAOD::ShallowAuxContainer* electrons_aux(nullptr);
  ANA_CHECK( m_SUSYTools->GetElectrons(electrons, electrons_aux, true, "Electrons"));
  
  // Photon
  xAOD::PhotonContainer* photons(nullptr);
  xAOD::ShallowAuxContainer* photons_aux(nullptr);
  ANA_CHECK( m_SUSYTools->GetPhotons(photons, photons_aux, true, "Photons"));
  
  // Muon
  xAOD::MuonContainer* muons(nullptr);
  xAOD::ShallowAuxContainer* muons_aux(nullptr);
  ANA_CHECK( m_SUSYTools->GetMuons(muons, muons_aux, true, "Muons"));

  // Jet
  xAOD::JetContainer* jets(nullptr);
  xAOD::ShallowAuxContainer* jets_aux(nullptr);
  ANA_CHECK( m_SUSYTools->GetJets(jets, jets_aux, true, ""));

  // Tau
  xAOD::TauJetContainer* taus(nullptr);
  xAOD::ShallowAuxContainer* taus_aux(nullptr);
  ANA_CHECK( m_SUSYTools->GetTaus(taus, taus_aux, true, "TauJets"));

  xAOD::MissingETContainer* mettst = new xAOD::MissingETContainer;
  xAOD::MissingETAuxContainer* mettst_aux = new xAOD::MissingETAuxContainer;
  mettst->setStore(mettst_aux);
  mettst->reserve(10);
  double metsig_tst(0.0);
  ANA_CHECK( m_SUSYTools->GetMET(*mettst, jets, electrons, muons, photons, taus, true, true));
  ANA_CHECK( m_SUSYTools->GetMETSig(*mettst, metsig_tst, true, true));

  // Overlap Removal
  ANA_CHECK( m_SUSYTools->OverlapRemoval(electrons, muons, jets, photons, taus) );

  bool passDisapTrig_medium = m_SUSYTools->IsTrigPassed("HLT_xe80_tcpufit_distrk20_medium_L1XE50");
  bool passDisapTrig_tight  = m_SUSYTools->IsTrigPassed("HLT_xe80_tcpufit_distrk20_tight_L1XE50");
    
  //Good Objects                                                                                                                                                                                                    
  xAOD::JetContainer*      goodJets      = new xAOD::JetContainer(SG::VIEW_ELEMENTS);
  xAOD::ElectronContainer* goodElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
  xAOD::MuonContainer*     goodMuons     = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
  xAOD::TauJetContainer*   goodTaus      = new xAOD::TauJetContainer(SG::VIEW_ELEMENTS);
  xAOD::PhotonContainer*   goodPhotons   = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
  
  // Good Jets
  for(const auto& jet : *jets){
    if(jet->auxdata<char>("baseline")==1 && jet->auxdata<char>("passOR")==1 && jet->auxdata<char>("signal")==1){
      goodJets->push_back(jet);
    }// if
  }// for jets
  
  // Good Electrons
  for(const auto& ele : *electrons){
    if(ele->auxdata<char>("baseline")==1 && ele->auxdata<char>("passOR")==1 && ele->auxdata<char>("signal")==1){
      goodElectrons->push_back(ele);
    }// if
  }// for electrons
  
  // Good Muons
  for(const auto& muon : *muons){
    if(muon->auxdata<char>("baseline")==1 && muon->auxdata<char>("passOR")==1 && muon->auxdata<char>("signal")==1){
      goodMuons->push_back(muon);
    }// if
  }// for muons
  
  // Good Taus
  for(const auto& tau : *taus){
    if(tau->auxdata<char>("baseline")==1 && tau->auxdata<char>("passOR")==1){
      goodTaus->push_back(tau);
    }// if
  }// for taus
  
  // Good Photons
  for(const auto& photon : *photons){
    if(photon->auxdata<char>("baseline")==1 && photon->auxdata<char>("passOR")==1 && photon->auxdata<char>("signal")==1){
      goodPhotons->push_back(photon);
    }// if
  }// for photons
  
  xAOD::TrackParticleContainer *stdTracks = new xAOD::TrackParticleContainer(SG::VIEW_ELEMENTS);
  xAOD::TrackParticleContainer *disapTracks = new xAOD::TrackParticleContainer(SG::VIEW_ELEMENTS);
  xAOD::ShallowAuxContainer *stdTracks_aux = new xAOD::ShallowAuxContainer(SG::VIEW_ELEMENTS);
  xAOD::ShallowAuxContainer *disapTracks_aux = new xAOD::ShallowAuxContainer(SG::VIEW_ELEMENTS);
  xAOD::TrackParticleContainer *goodStdTracks = new xAOD::TrackParticleContainer(SG::VIEW_ELEMENTS);

  const xAOD::TrackParticleContainer *tracks_std(0);
  const xAOD::TrackParticleContainer *tracks_disap(0);

  ANA_CHECK(m_event->retrieve(tracks_std, "InDetTrackParticles"));
  std::pair<xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer*> shallowcopy_std = xAOD::shallowCopyContainer(*tracks_std);
  stdTracks = xAOD::shallowCopyContainer(*tracks_std).first;
  stdTracks_aux = xAOD::shallowCopyContainer(*tracks_std).second;
  
  ANA_CHECK(m_event->retrieve(tracks_disap, "InDetDisappearingTrackParticles"));
  std::pair<xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer*> shallowcopy_disap = xAOD::shallowCopyContainer(*tracks_disap);
  disapTracks = xAOD::shallowCopyContainer(*tracks_disap).first;
  disapTracks_aux = xAOD::shallowCopyContainer(*tracks_disap).second;

  for(const auto& track : *stdTracks){
    if(m_trackSelectionTool->accept(track)){
      goodStdTracks->push_back(track);

      if(iEntry==0){
	float pixeldEdx = 0.0;
	track->summaryValue(pixeldEdx, xAOD::pixeldEdx);
	std::bitset<MyUtil::DetectorType::numberOfDetectorTypes> hitPattern = track->hitPattern();

	std::cout << "pixeldEdx  : " << pixeldEdx << std::endl;
	std::cout << "hitPattern : " << hitPattern << std::endl;
	std::cout << "Is hit on SCT barrel 0 ? : " << hitPattern[MyUtil::DetectorType::sctBarrel0] << std::endl;
      }
    }
  }// for stdTracks

  m_transientStorage->clear();
  
  return 0;
}

void Run3Ana::Run()
{
  //using namespace asg::msgRun3Ana;
  if (begin()){
    return;
  }
  ANA_MSG_INFO("Run3Ana::Run::start");

  for(ULong64_t iEntry=0;iEntry<nEntries;iEntry++){
    //if(iEntry==100)
    //break;
    processEvent(iEntry);
  }// for iEntry

  ANA_MSG_INFO("Run3Ana::Run::end");
  finish();

}
