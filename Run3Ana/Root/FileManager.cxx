
//MYCODE
#include "Run3Ana/FileManager.h"

//ROOT
#include "TFile.h"

//SYSTEM
#include <string>
#include <vector>
#include <iostream>

FileManager::FileManager(const std::vector<std::string> list)
: APP_NAME("FileManager")
{  
  m_fileList = list;
  std::cout << "nFiles : " << m_fileList.size() << std::endl;
  m_curIndex = 0;
}

FileManager::FileManager(const char* list)
: APP_NAME("FileManager")
{
    m_fileList = SeparateCommaString(list);
    if(m_fileList.size() == 0){ std::cout<<"FileManager::FileManager :: No input files"<<std::endl;}
    m_curIndex = 0;
}

void FileManager::Initialize(){
    m_curIndex = 0;
}

std::string FileManager::nextFile(){
    m_curIndex++;
    if(m_curIndex == m_fileList.size()) return "";
    return curFile();
}

std::string FileManager::curFile() const{
    if(m_curIndex == m_fileList.size()) return "";
    return m_fileList.at(m_curIndex);
}

std::vector<std::string> FileManager::SeparateCommaString(std::string fileArray){
    std::string inputfileArray = std::string(fileArray);
    std::vector<std::string> inputfileList;

    while (true){
        std::string::size_type pos = inputfileArray.find(",");
        if (pos != std::string::npos){
            inputfileList.push_back(inputfileArray.substr(0, pos));
            inputfileArray.erase(0, pos + 1);
        }
        else{
            inputfileList.push_back(inputfileArray);
            break;
        }
    }

    return inputfileList;
}

bool FileManager::checkRootFile(const TFile *file, std::string filename){
    if ( !file ) {
        std::cout<<"Failed to include inputFile : " << filename << std::endl;
        return false;
    }
    if ( file->IsZombie() ) {
        std::cout<<"Include inputFile is Zombie. don't read this file and read next file : " << filename << std::endl;
        return false;
    }
    return true;
}

bool FileManager::GetNextFile(TFile*& file){
    SafeDeleteTFile(file);
    
    // try to get next file
    std::string nextFileName = nextFile();
    std::cout << "next InputFile : " << nextFileName << std::endl;;
    if ( nextFileName.empty() ) {
        std::cout<< "FileManager :: No input file" << std::endl;
        return false;
    }
    OpenFile(file, nextFileName);
    return true;
}

void FileManager::OpenFile(TFile*& file, std::string filename){
    std::cout<<"Open input file : "<<filename<<std::endl;;
    file = TFile::Open(filename.c_str(), "READ");
}

void FileManager::SafeDeleteTFile(TFile*& file){
    if(file!=nullptr){
        file->Close();
        delete file;
        file = nullptr;
    }
}

