//MYCODE
#include "Run3Ana/MyConfig.h"
#include "Run3Ana/FileManager.h"

//ATLAS(TOOL)
#include "PathResolver/PathResolver.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"

//ROOT
#include <TEnv.h>

//SYSTEM
#include <string>
#include <iostream>
 
int MyConfig::Initialize()
{
    TEnv rEnv;
    auto absFilePath = PathResolverFindCalibFile(m_myConfigName);
    if(absFilePath=="") return -1;
    int success = rEnv.ReadFile(absFilePath.c_str(), kEnvAll);
    if (success != 0) return -1;

    std::cout << "Config file \"" << m_myConfigName << "\" for disappearing track analysis opened" << std::endl;

    m_maxNEvents                   = rEnv.GetValue("General.maxNEvents",   -1);
    m_doGRL                        = rEnv.GetValue("Tool.doGRL",                   (int) true);
    m_doPRW                        = rEnv.GetValue("Tool.doPRW",                   (int) true);
    m_IsData                       = rEnv.GetValue("SampleInfo.IsData",            (int) false);
    m_stConfigLocation             = rEnv.GetValue("SUSYTools.ConfigFileLocation", "Run3Ana/Default.conf");

    std::string grlLocation_tmp    = rEnv.GetValue("Tool.GRLLocation", "MyAnalysis/GRL/data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardGRL_All_Good.xml");
    auto list = FileManager::SeparateCommaString(grlLocation_tmp);
    for(auto loc : list){
        m_grlLocation.push_back(PathResolverFindCalibFile(loc));
    }

    m_prwConfigLocation   = rEnv.GetValue("Tool.PRWConfigLocation",   "MyAnalysis/PileupReweighting/mc15ab_defaults.NotRecommended.prw.root");
    if(m_prwConfigLocation != "auto"){
        m_prwConfigLocation   = PathResolverFindCalibFile(m_prwConfigLocation);
    }
    m_prwLumiCalcLocation = rEnv.GetValue("Tool.PRWLumiCalcLocation", "MyAnalysis/PileupReweighting/ilumicalc_histograms_None_276262-284484.root");
    if(m_prwLumiCalcLocation != "auto"){
      m_prwLumiCalcLocation = PathResolverFindCalibFile(m_prwLumiCalcLocation);
    }
    
    
    //==========================================//
    //=== Check If Config Files Are Complete ===//
    //==========================================//
    std::vector<std::string> configVariableList = {
        "General.maxNEvents",
        "SampleInfo.IsData",
        "Tool.doGRL",
        "Tool.doPRW",
        "Tool.GRLLocation",
        "Tool.PRWConfigLocation",
        "Tool.PRWLumiCalcLocation",
        "SUSYTools.ConfigFileLocation",
    };
    for(auto configVariable: configVariableList){
        if(rEnv.Lookup(configVariable.c_str())==0){
            std::cout<< "readConfig : config variable " << configVariable << " is not written in config file. Set Defalt value." <<std::endl;
        }
    }
    
  return 0;
}

void MyConfig::Print() const{
  std::cout << "General : " << std::endl;
  std::cout << "  maxNEvents                     : " << m_maxNEvents       << std::endl;

  std::cout << "Input files : " << std::endl;
  std::cout << "  IsData                         : " << m_IsData           << std::endl;

  std::cout << "SUSYTools Configuration : " << std::endl;
  std::cout << "  SUSYToolsConfigFileLocation    : " << m_stConfigLocation << std::endl;
}

