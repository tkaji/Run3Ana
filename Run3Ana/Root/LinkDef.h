#ifndef Run3Ana_LinkDef_h
#define Run3Ana_LinkDef_h

//#include <Run3Ana/Run3Ana.h>
//#include <Run3Ana/MyConfig.h>

//ROOT
#include <TLorentzVector.h>
#include <TClonesArray.h>

//SYSTEM
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <stdint.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//#pragma link C++ class std::vector<TLorentzVector>+;


#endif

#endif
