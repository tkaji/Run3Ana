#include "AnaClus.h"

//ATLAS (EDM)
//#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODBase/IParticleHelpers.h"
//#include "xAODRootAccess/tools/TReturnCode.h"
#include "xAODMetaData/FileMetaData.h"

//ATLAS (TOOL)
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PathResolver/PathResolver.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "PATInterfaces/CorrectionCode.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"

//#include "TEfficiency.h"

//SYSTEM
#include <sys/resource.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

namespace asg{ANA_MSG_HEADER(msgAnaClus) ANA_MSG_SOURCE(msgAnaClus,"AnaClus")}
using namespace asg::msgAnaClus;

int main(int argc, char **argv)
{
  po::options_description desc("option description");
  
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("output-file,o", po::value<std::string>()->default_value("out.root"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  std::vector<std::string> InputFileList = vm["input-files"].as<std::vector<std::string> >();
  
  //fin = vm["input-files"].as<std<std::string> >();
    
  m_event.reset(new xAOD::TEvent(xAOD::TEvent::kClassAccess));
  //curFile = TFile::Open(inFile.c_str(), "READ");

  if(vm["grid"].as<int>()==0){
    m_FileManager.reset(new FileManager(InputFileList));    
  }else{
    m_FileManager.reset(new FileManager(InputFileList.at(0).c_str()));
  }
  m_transientStorage.reset(new xAOD::TStore());
  m_transientStorage->setActive();

  m_FileManager->Initialize();

  //gROOT->cd();
  hTrack_pT = new TH1D("hTrack_pT", ";p_{T} [GeV];Entries", 200, 0, 2);
  hTrack_DEDX = new TH1D("hTrack_DEDX", ";dE/dx;Entries", 200, 0, 10);
  hTrack_Phi = new TH1D("hTrack_Phi", ";#phi;Entries", 200, -TMath::Pi(), TMath::Pi());
  hTrack2D_DEDX_pT = new TH2D("hTrack2D_DEDX_pT", ";p_{T} [GeV];dE/dx", 200, 0, 2, 200, 0, 10);
  hTrack2D_DEDX_Phi = new TH2D("hTrack2D_DEDX_Phi", ";p_{T} [GeV];dE/dx", 200, -TMath::Pi(), TMath::Pi(), 200, 0, 10);
  hTruth_PDGID = new TH1D("hTruth_PDGID", ";PDGID", 10000, 0, 10000);

  hTruth_DeltaZ = new TH1D("hTruth_DeltaZ", ";#Delta Z_{truth, track}", 200, 0, 20);

  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
  while(!m_FileManager->curFile().empty()){
    if ( !curFile ) FileManager::OpenFile(curFile, m_FileManager->curFile());
    ANA_CHECK(m_event->readFrom(curFile));
      
    Long64_t iEntry = 0;
    Long64_t nEntries = m_event->getEntries();
    std::cout << "nEntries : " << nEntries << std::endl;
    //while(iEntry < nEntries){
    while(iEntry < 10){ // Only 10 events for debug
      m_event->getEntry(iEntry);
	
      // Read Track Information
      const xAOD::TrackParticleContainer *stdTracks = nullptr;
      const xAOD::TrackParticleContainer *disapTracks = nullptr;

      const xAOD::TruthParticleContainer *Truths = nullptr;
      
      ANA_CHECK(m_event->retrieve(stdTracks, "InDetTrackParticles"));
      ANA_CHECK(m_event->retrieve(disapTracks, "InDetDisappearingTrackParticles"));
      ANA_CHECK(m_event->retrieve(Truths, "TruthParticles"));

      for(const auto& track : *stdTracks){
	track->auxdecor<int>("MyTruthIndex") = -1;
      }
      int truthIndex=-1;
      for(const auto& truth : *Truths){
	truthIndex++;
	//std::cout << "status : " << truth->status() << ", PDGID : " << truth->absPdgId() << std::endl;
	bool fDoTruthMatch=false;
	if(truth->status()==1){
	  hTruth_PDGID->Fill(truth->absPdgId());
	  if(truth->pdgId()==2212){
	    // proton
	    fDoTruthMatch=true;
	  }else if(truth->pdgId()==-2212){
	    // anti-proton
	    fDoTruthMatch=true;	    
	  }// specific particle
	}// status==1

	if(fDoTruthMatch){
	  double dR = 0.10;
	  double mindR = dR;
	  int minIndex = -1;

	  int tmpIndex = -1;
	  for(const auto& track : *stdTracks){
	    tmpIndex++;	    
	    double tmpdR = xAOD::P4Helpers::deltaR(truth, track, false);
	    if(TMath::Abs(truth->prodVtx()->z()-track->z0()) < 5.0){
	      if(tmpdR < mindR){
		mindR = tmpdR;
		minIndex = tmpIndex;
	      }
	    }
	  }// for track
	  if(minIndex!=-1){
	    stdTracks->at(minIndex)->auxdecor<int>("MyTruthIndex") = truthIndex;
	    //std::cout << "truth Index = " << tmpCount << std::endl;
	    //std::cout << "debug PDGID = " << truth->pdgId() << ", Index = " << minIndex << std::endl;
	    
	    /*
	    std::cout << "mindR = " << mindR << std::endl;
	    std::cout << "dR1   = " << xAOD::P4Helpers::deltaR(truth, stdTracks->at(minIndex), true) <<std::endl;
	    std::cout << "dR2   = " << xAOD::P4Helpers::deltaR(truth, stdTracks->at(minIndex), false) << std::endl;	    
	    std::cout << "truth(eta, phi) =  (" << truth->eta() << ", " << truth->phi() << ")" <<std::endl;
	    std::cout << "track(eta, phi) =  (" << stdTracks->at(minIndex)->eta() << ", " << stdTracks->at(minIndex)->phi() << ")" <<std::endl;
	    std::cout << "phi = " << (*stdTracks->at(minIndex)->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink"))->phi() << std::endl;
	    std::cout << "truth z = " << truth->prodVtx()->z() << std::endl;
	    std::cout << "track z = " << stdTracks->at(minIndex)->z0() << std::endl;
	    */
	    /*
	    hTruth_DeltaZ->Fill(TMath::Abs(truth->prodVtx()->z()-stdTracks->at(minIndex)->z0()));
	    ElementLink<xAOD::TruthParticleContainer> link = stdTracks->at(minIndex)->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
	    if(link.isValid()){
	      if(TMath::Abs(truth->prodVtx()->z()-stdTracks->at(minIndex)->z0())>3.0){
		std::cout << "PDGID = " << (*link)->pdgId() << std::endl;
	      }
	    }
	    */
	  }
	}	
      }// for truth

      for(const auto& track : *stdTracks){
	float pixeldEdx = 0.0;
	track->summaryValue(pixeldEdx, xAOD::pixeldEdx);
	hTrack_pT->Fill(track->pt()/1000.0);
	hTrack_DEDX->Fill(pixeldEdx);
	hTrack_Phi->Fill(track->phi());
	hTrack2D_DEDX_pT->Fill(track->pt()/1000.0, pixeldEdx);
	hTrack2D_DEDX_Phi->Fill(track->phi(), pixeldEdx);
	
	if(iEntry==0){
	  std::bitset<MyUtil::DetectorType::numberOfDetectorTypes> hitPattern = track->hitPattern();

	  std::cout << "pixeldEdx  : " << pixeldEdx << std::endl;
	  std::cout << "hitPattern : " << hitPattern << std::endl;
	  std::cout << "Is hit on SCT barrel 0 ? : " << hitPattern[MyUtil::DetectorType::sctBarrel0] << std::endl;
	  std::cout << "MyTruth Index : " << track->auxdecor<int>("MyTruthIndex") << std::endl;
	  std::cout << "MyTruth PDGID : " << (track->auxdecor<int>("MyTruthIndex")!=-1 ? Truths->at(track->auxdecor<int>("MyTruthIndex"))->pdgId() : -1) << std::endl;
	  ElementLink<xAOD::TruthParticleContainer> link = track->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
	  std::cout << "Official Truth PDGID : " << (link.isValid() ? (*link)->pdgId() : -1) << std::endl;
	}

	if( ! track->isAvailable< MeasurementsOnTrack >("Reco_msosLink")){
	  std::cout << "MSOS link is not available!!" << std::endl;
	  continue;
	}
	const MeasurementsOnTrack& measurementsOnTrack = track->auxdata<MeasurementsOnTrack>("Reco_msosLink");	  
	for(auto msos_iter = measurementsOnTrack.begin(); msos_iter != measurementsOnTrack.end(); ++msos_iter){
	  if( ! (*msos_iter).isValid() ) continue;
	  const xAOD::TrackStateValidation* msos = *(*msos_iter);
	  if( !(msos->type() == 0 && msos->detType() == 1))  continue; // type==0: cluster used for tracking?, detType==1: Pixel

	  const ElementLink<xAOD::TrackMeasurementValidationContainer> trkMeasurement = msos->trackMeasurementValidationLink();
	  if( !(trkMeasurement.isValid() && *trkMeasurement!=nullptr )) continue;
	  if ( ! (*trkMeasurement)->isAvailable <float > ("charge") ) continue;
	  std::cout << "charge = " << (*trkMeasurement)->auxdataConst<float>("charge") << std::endl;

	  int eta_module = (*trkMeasurement)->auxdataConst<int>("eta_module");
	  int layer = (*trkMeasurement)->auxdataConst<int>("layer");
	  int bec   = (*trkMeasurement)->auxdataConst<int>("bec");
	  float charge = (*trkMeasurement)->auxdataConst<float>("charge");
	  double locx = (*trkMeasurement)->auxdataConst<float>("localX");
	  double locy = (*trkMeasurement)->auxdataConst<float>("localY");
	  float msosTheta = msos->localTheta();
	  float msosPhi   = msos->localPhi();
	  float alpha = std::atan(std::hypot(std::tan(msosTheta),std::tan(msosPhi)));

	  // dEdx calculation
	  float cosalpha = std::cos(alpha);
	  if (std::abs(cosalpha)<0.16)
	    continue;
	  
	  double dEdxValue = charge * cosalpha;
	  
	  if(bec==0 && layer==0){ // IBL
	    if(((eta_module >= -10 && eta_module <= -7) || (eta_module >= 6 && eta_module <= 9)) && (fabs(locy) < 10. && (locx > -8.33 && locx < 8.3))){ // Good cluster for IBL 3D module
	      dEdxValue *= conversionfactor/IBL_3D_sensorthickness;
	    }else if ((eta_module >= -6 && eta_module <= 5) && (fabs(locy) < 20. && (locx > -8.33 && locx < 8.3))) { // Good cluster for IBL planar
	      dEdxValue *= conversionfactor/IBL_PLANAR_sensorthickness;
	    }else{
	      continue;
	    }
	  }else if(bec==0 && fabs(locy)<30. &&  ((locx>-8.20 && locx<-0.60) || (locx>0.50 && locx<8.10))) {
	    dEdxValue=charge*conversionfactor/Pixel_sensorthickness;
	  }else if (std::abs(bec)==2 && fabs(locy)<30. && ((locx>-8.15 && locx<-0.55) || (locx>0.55 && locx<8.15))) {
	    dEdxValue=charge*conversionfactor/Pixel_sensorthickness;	    
	  }else{
	    continue;
	  }
	  // Current code does not consider about overflow
	  std::cout << Form("(layer, bec, dEdx) = (%d, %d, %.3lf)", layer, bec, dEdxValue) << std::endl;	  
	}// for msos_iter
	
      }// for stdTracks
      m_transientStorage->clear();
      iEntry++;
    }// while

            
    FileManager::SafeDeleteTFile(curFile);
    m_FileManager->nextFile();
  } // while
  //FileManager::SafeDeleteTFile(curFile);
  
  //gROOT->cd();
  //return 0;
  outFile = new TFile(vm["output-file"].as<std::string>().c_str(), "RECREATE");
  //outFile.reset(new TFile(vm["output-file"].as<std::string>().c_str(), "RECREATE"));
  hTrack_pT->Write("", TObject::kOverwrite);
  hTrack_DEDX->Write("", TObject::kOverwrite);
  hTrack_Phi->Write("", TObject::kOverwrite);
  hTrack2D_DEDX_pT->Write("", TObject::kOverwrite);
  hTrack2D_DEDX_Phi->Write("", TObject::kOverwrite);
  hTruth_PDGID->Write("", TObject::kOverwrite); // It has proton, pion, sigma, neutron, and so on...
  hTruth_DeltaZ->Write("", TObject::kOverwrite);
  outFile->Close(); delete outFile; outFile=nullptr;
  
  std::cout << "finish AnaClus" << std::endl;

  return 0;
}
