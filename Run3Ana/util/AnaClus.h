#ifndef __Run3Ana_AnaClus_H__
#define __Run3Ana_AnaClus_H__

#include <iostream>
#include <fstream>
#include <getopt.h>
#include <cstdlib>
#include <string>

#include "TSystem.h"
#include "Run3Ana/Utility.h"
#include "Run3Ana/FileManager.h"
#include "AnaClus.h"


//ATLAS (EDM)
#include "xAODEventInfo/EventInfo.h"
//#include "xAODEgamma/ElectronContainer.h"
//#include "xAODMuon/MuonContainer.h"
//#include "xAODTruth/TruthParticleContainer.h"
//#include "xAODTracking/TrackParticleContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "FourMomUtils/xAODP4Helpers.h" 
#include "AsgMessaging/StatusCode.h"
#include "AsgMessaging/MessageCheck.h"
#include "AsgTools/ToolStore.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/StandaloneToolHandle.h"
#include "AsgTools/IAsgTool.h"
#include <xAODTracking/TrackStateValidationContainer.h>
//ROOT
#include <Rtypes.h>
#include "Rtypes.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"

//SYSTEM
#include <string>
#include <stdint.h>
#include <memory>
#include <map>

//ATLAS (EDM)
namespace xAOD { class TEvent; }
namespace xAOD { class TStore; }
namespace  InDet{class InDetTrackSelectionTool;}
//ROOT
class TFile;
class TEfficiency;


const float energyPair       = 3.68e-6;  // Energy in MeV to create an electron-hole pair in silicon
const float sidensity        = 2.329;    // silicon density in g cm^-3
const float conversionfactor = energyPair / sidensity;

const float IBL_PLANAR_sensorthickness = 0.0200; // thickness for IBL planar [cm] = 200 um
const float IBL_3D_sensorthickness  = 0.0230;    // thickness for IBL 3D [cm]     = 230 um
const float Pixel_sensorthickness = 0.0250;      // thickness for PIXEL [cm]      = 250 um

std::shared_ptr<xAOD::TEvent> m_event;
std::vector<std::string> m_FileList;
std::unique_ptr<FileManager> m_FileManager;
std::shared_ptr<xAOD::TStore> m_transientStorage;
TFile *curFile;

TFile *outFile;
//std::shared_ptr<TFile> outFile;

TH1D *hTrack_pT;
TH1D *hTrack_Eta;
TH1D *hTrack_Phi;

TH1D *hTrack_DEDX;

TH2D *hTrack2D_DEDX_pT;
TH2D *hTrack2D_DEDX_Phi;

TH1D *hTruth_PDGID;
TH1D *hTruth_DeltaZ;

#endif // __Run3Ana_AnaClus_H__
