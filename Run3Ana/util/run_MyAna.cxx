#include <iostream>
#include <fstream>
#include <getopt.h>
#include <cstdlib>
#include <string>

#include "TSystem.h"
#include "Run3Ana/Run3Ana.h"

int main(int argc, char **argv)
{
    int opt;
    std::string fin = "";
    std::string ofile = "output.root";
    std::string dataType;
    std::string myconfig="MyAnalysis/MyAnalysis_Default.conf";

    if (argc>1){
        while ((opt = getopt(argc, argv, "i:o:c:h")) != -1){
            switch (opt){
            case 'h':
                std::cout << "-i --> inputFileName" << std::endl;
                std::cout << "-o --> outputFileName" << std::endl;
                std::cout << "-c --> configFileName" << std::endl;
                return 0;
            case 'i':
                fin = optarg;
                break;
            case 'o':
                ofile = optarg;
                break;
            case 'c':
                myconfig = optarg;
                break;
            }
        }
    }
    else{
        std::cerr << "no input file" << std::endl; exit(1);
    }

    Run3Ana*  MyAna = new Run3Ana("MyRun3Ana", fin.c_str(), ofile.c_str(), myconfig.c_str());
    
    std::cout << "begin MyAna" << std::endl; 
    MyAna->Run();
    std::cout << "finish MyAna" << std::endl;
    delete MyAna;

    return 0;
}
