# Run3Ana



## Getting started

```
setupATLAS
git clone ssh://git@gitlab.cern.ch:7999/tkaji/Run3Ana.git
mkdir build
mkdir run
cd build/
source ../Run3Ana/asetup.sh
cmake ../Run3Ana/
make -j
source x86_64-el9-*/setup.sh
cd ../run/
run_MyAna -i /gpfs/fs6001/toshiaki/DAOD_LLP1/mc23_13p6TeV.601190.PhPy8EG_AZNLO_Zmumu.deriv.DAOD_LLP1.e8514_s4159_r15224_p6215/DAOD_LLP1.39017166._000334.pool.root.1 -c ../Run3Ana/Run3Ana/data/Default_MC.conf
```

For cluster study

```
AnaClus ../../../AOD/user.tkaji.MCGen.999999.single_proton_Eta0_Pt100_5000MeV.AOD.20240624_1_EXT0/user.tkaji.40071208.EXT0._000001.AOD.pool.root
```
